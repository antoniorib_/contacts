package br.senai.sp.informatica.contacts.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.contacts.R;
import br.senai.sp.informatica.contacts.adapter.SimplesAdapter;
import br.senai.sp.informatica.contacts.dao.ContactDao;
import br.senai.sp.informatica.contacts.model.Contact;

public class ContactListActivity extends BaseActivity {

    ListView listViewContacts;
    ContactDao contactDao;
    List<Contact> contacts = new ArrayList<>();
    Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        startComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadList();
    }

    public void newContactOnClick(View view){
        Intent intent = new Intent(this, ContactSaveActivity.class);
        startActivity(intent);
    }

    @Override
    public void startComponents() {
        listViewContacts = (ListView) findViewById(R.id.contact_list);
        loadList();
    }

    private void loadList(){
        //Creates an instance of dao
        contactDao = new ContactDao(this);
        //Fills the array list with table data
        contacts = contactDao.findAll();
        //Creates an instance of adapter, informing the context and the contact list
        SimplesAdapter adapter = new SimplesAdapter(this, contacts);
        //Associates the arraylist to the adapter
        listViewContacts.setAdapter(adapter);
    }
}
