package br.senai.sp.informatica.contacts.activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.senai.sp.informatica.contacts.R;
import br.senai.sp.informatica.contacts.activity.BaseActivity;
import br.senai.sp.informatica.contacts.dao.ContactDao;
import br.senai.sp.informatica.contacts.model.Contact;
import br.senai.sp.informatica.contacts.model.CreditCard;
import br.senai.sp.informatica.contacts.util.MessageUtil;

public class ContactSaveActivity extends BaseActivity {

    //Lista de cartões de crédito
    List<String> creditCardList = new ArrayList<>();
    Spinner creditCardSpinner;

    int[] cardImages = new int[]{
            R.drawable.diners,
            R.drawable.visa,
            R.drawable.master,
            R.drawable.american_express,
            R.drawable.elo,
            R.drawable.hiper
    };

    ImageView imageCard;

    Button buttonLastCall;
    int day, month, year;
    Locale locale = new Locale("pt", "BR");
    DatePickerDialog datePickerDialog;
    EditText editName;
    EditText editEmail;
    EditText editPhone;
    RadioButton radioMale;
    RadioButton radioFema;
    CheckBox checkClient;

    Contact contact = null;
    ContactDao contactDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_save);
        startComponents();
        loadCardList();
        loadCardSpinner();
        setLastCallButton();
    }

    @Override
    public void startComponents() {
        creditCardSpinner = (Spinner) findViewById(R.id.credit_card_spinner);
        creditCardSpinner.setOnItemSelectedListener(onCardSelectedListener());
        imageCard = (ImageView) findViewById(R.id.image_card);
        buttonLastCall = (Button) findViewById(R.id.button_last_call);
        editName = (EditText) findViewById(R.id.edit_name);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        radioMale = (RadioButton) findViewById(R.id.radioMasc);
        radioFema = (RadioButton) findViewById(R.id.radioFem);
        checkClient = (CheckBox) findViewById(R.id.check_client);
    }

    private void setLastCallButton(){
        //Today's date
        Calendar calendar = Calendar.getInstance();
        day = calendar.get(calendar.DAY_OF_MONTH);
        month = calendar.get(calendar.MONTH);
        year = calendar.get(calendar.YEAR);
        buttonLastCall.setText(String.format(locale, "%02d/%02d/%04d", day, month+1, year));
        //Opens the DatePicker in the actual date
        datePickerDialog = new DatePickerDialog(this, datePickerListener(), year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener(){
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year2, int month2, int dayOfMonth) {
            //Alters the variables from date
                day = dayOfMonth;
                month = month2;
                year = year2;
                //Defines the new date as a text of the button
                buttonLastCall.setText(String.format(locale, "%02d/%02d/%04d", day, month+1, year));
            }
        };
    }

    public void lastCallOnClick(View view){
        //Opens the datepicker
        datePickerDialog.show();
    }

    private AdapterView.OnItemSelectedListener onCardSelectedListener(){
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Changes the image of ImageView
                imageCard.setImageResource(cardImages[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    /**
     * This Method loads the credit card list using data from ENUM
     */
    private void loadCardList(){
        //For Each credit card in ENUM, add to ArrayList
        for (CreditCard c: CreditCard.values()) {
            creditCardList.add(c.name());
        }
    }

    /**
     * This Method sets data from credit card list into Spinner
     */
    private void loadCardSpinner(){
        //Creates an adapter for the data of the Spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, creditCardList);

        //Associates the Spinners to the adapter
        creditCardSpinner.setAdapter(adapter);
    }

    public void contactSaveOnClick(View view){
        if (validate()){
            if(contact == null){
                contact = new Contact();
            }
            contact.setName(editName.getEditableText().toString());
            contact.setEmail(editEmail.getEditableText().toString());
            contact.setPhone(editPhone.getEditableText().toString());
            contact.setCreditCard(creditCardSpinner.getSelectedItem().toString());
            if(radioFema.isChecked()){
                contact.setGender("Female");
            }else{
                contact.setGender("Male");
            }
            //Last Contact
            Calendar lastCall = Calendar.getInstance();
            //define a data
            lastCall.set(year, month, day);
            contact.setLastCall(lastCall);
            if(checkClient.isChecked()){
                contact.setClient(true);
            }else{
                contact.setClient(false);
            }
            contactDao = new ContactDao(this);

            //Checks if it was saved
            if(contactDao.save(contact) != -1){
                //success IZIIIIIII
                MessageUtil.toast(this, getString(R.string.success_save));
            }else{
                //And you failed
                MessageUtil.toast(this, getString(R.string.error_save));
            }
            resetForm();
        }
    }

    private boolean validate(){
        //Verifies if the fields are empty
        if(editName.getEditableText().toString().isEmpty()){
            MessageUtil.toast(this, getString(R.string.fill_name));
            editName.requestFocus();
            return  false;
        }else if (editEmail.getEditableText().toString().isEmpty()){
            MessageUtil.toast(this, getString(R.string.fill_email));
            editEmail.requestFocus();
            return false;
        }else if (editPhone.getEditableText().toString().isEmpty()){
            MessageUtil.toast(this, getString(R.string.fill_phone));
            editPhone.requestFocus();
            return false;
        }else{
            return true;
        }
    }

    private void resetForm(){
        editName.setText("");
        editEmail.setText("");
        editPhone.setText("");
        loadCardList();
        loadCardSpinner();
        radioMale.setChecked(true);
        setLastCallButton();
        checkClient.setChecked(false);
        editName.requestFocus();
    }
}
