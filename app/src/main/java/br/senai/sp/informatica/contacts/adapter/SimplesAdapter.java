package br.senai.sp.informatica.contacts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import br.senai.sp.informatica.contacts.R;
import br.senai.sp.informatica.contacts.model.Contact;

/**
 * Created by Tecnico_Tarde on 27/03/2017.
 */

public class SimplesAdapter extends BaseAdapter {

    Context context;
    List<Contact> contacts;

    public SimplesAdapter(Context context, List<Contact> contacts){
        super();
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts != null ? contacts.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Creates display layout
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_simples, parent, false);
        //Creates a TextView to display the name of the contact
        TextView textView = (TextView) view.findViewById(R.id.contact_name);
        //Creates a contact for each position of the list
        Contact contact = contacts.get(position);
        //Defines the text of the TextView with the contact's name
        textView.setText(contact.getName());
        return view;
    }
}
