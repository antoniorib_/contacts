package br.senai.sp.informatica.contacts.dao;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import br.senai.sp.informatica.contacts.model.Contact;

/**
 * Created by Tecnico_Tarde on 20/03/2017.
 */

public class ContactDao implements DefaultDao<Contact> {

    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;
    private Context context;

    //Constructor
    public ContactDao(Context context) {
        this.context = context;
        //Creates an instace of DBHelper
        this.dbHelper = new DBHelper(context);
    }

    @Override
    public long save(Contact contact) {
        //Verifies if there is an instance of sqlDatabase
        if(sqLiteDatabase == null){
            //Opens the db file with read and write permissions. Equivalent to connection.opne()
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            ContentValues values = new ContentValues();
            values.put("name", contact.getName());
            values.put("email", contact.getEmail());
            values.put("phone", contact.getPhone());
            values.put("gender", contact.getGender());
            values.put("creditCard", contact.getCreditCard());
            values.put("lastCall", contact.getLastCall().getTimeInMillis());
            values.put("client", contact.isClient());
            return sqLiteDatabase.insertOrThrow("contact", "", values);
        } finally {
            //Closes the db file. Equivalent to connection.close()
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Contact> findAll() {
        //if database files are available
        if(sqLiteDatabase==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
        try{
            String sql = "SELECT * FROM contact";
            //forwards to a cursor the result of a consult
            Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
            return  toList(cursor);
        }finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public Contact find(String name) {
        return null;
    }

    @Override
    public long delete(Contact contact) {
        return 0;
    }

    @Override
    public List<Contact> toList(Cursor cursor) {
        //creates a list of Contacts
        List<Contact> contacts = new ArrayList<>();

        if (cursor.moveToFirst()){
            do{
                Contact contact = new Contact();
                contact.setId(cursor.getLong(cursor.getColumnIndex("contactID")));
                contact.setName(cursor.getString(cursor.getColumnIndex("name")));
                contact.setEmail(cursor.getString(cursor.getColumnIndex("email")));
                contact.setEmail(cursor.getString(cursor.getColumnIndex("phone")));
                contact.setCreditCard(cursor.getString(cursor.getColumnIndex("creditCard")));
                contact.setGender(cursor.getString(cursor.getColumnIndex("gender")));
                //last call
                //creates a calendar
                Calendar lastCall = Calendar.getInstance();
                //creates a date with table data
                Date data = new Date(cursor.getLong(cursor.getColumnIndex("lastCall")));
                lastCall.setTime(data);
                contact.setLastCall(lastCall);
                //----------------
                if (cursor.getInt(cursor.getColumnIndex("client")) > 0){
                    contact.setClient(true);
                }else{
                    contact.setClient(false);
                }

                contacts.add(contact);
            }while (cursor.moveToNext());
        }
        return contacts;
    }
}
