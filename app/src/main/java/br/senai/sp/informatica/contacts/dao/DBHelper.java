package br.senai.sp.informatica.contacts.dao;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by Tecnico_Tarde on 20/03/2017.
 */

public class DBHelper extends SQLiteOpenHelper{

    //Name of the schema
    public static final String DB_NAME = "contacts.db";

    //Version of the database
    public static final int DB_VERSION = 1;

    //Debug TAG
    public static final String TAG = "CONTACT_APP";

    //Constructor
    public DBHelper(Context context) {
        //A schema is created when a DBHelper is created
        super(context, DB_NAME, null, DB_VERSION);
        //super invokes the constructor of the class from which we heir.
    }

    //Creates the Table
    public static final String SQL_CONTACT = "CREATE TABLE IF NOT EXISTS contact (" +
                                            "contactID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                            "name TEXT NOT NULL," +
                                            "email TEXT NOT NULL UNIQUE COLLATE NOCASE," +
                                            "phone TEXT NOT NULL," +
                                            "gender TEXT NOT NULL, " +
                                            "creditCard TEXT NOT NULL, " +
                                            "lastCall NUMERIC NOT NULL, " +
                                            "client NUMERIC NOT NULL);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
