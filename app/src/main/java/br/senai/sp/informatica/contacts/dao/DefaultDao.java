package br.senai.sp.informatica.contacts.dao;

import android.database.Cursor;

import java.util.List;

/**
 * Created by Tecnico_Tarde on 20/03/2017.
 */

public interface DefaultDao<T> {
    public long save(T t);

    public List<T> findAll();

    public T find(String name);

    public long delete(T t);

    public List<T> toList(Cursor cursor);
}
