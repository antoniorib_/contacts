package br.senai.sp.informatica.contacts.model;

import java.util.Calendar;

/**
 * Created by Tecnico_Tarde on 20/03/2017.
 */

public class Contact {
    private long id;
    private String name;
    private String email;
    private String phone;
    private String creditCard;
    private String gender;
    private Calendar lastCall;
    private boolean client;

    public Contact() {
    }

    public Contact(long id, String name, String email, String phone, String creditCard, String gender, Calendar lastCall, boolean client) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.creditCard = creditCard;
        this.gender = gender;
        this.lastCall = lastCall;
        this.client = client;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Calendar getLastCall() {
        return lastCall;
    }

    public void setLastCall(Calendar lastCall) {
        this.lastCall = lastCall;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
