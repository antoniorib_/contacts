package br.senai.sp.informatica.contacts.model;

/**
 * Created by Tecnico_Tarde on 20/03/2017.
 */

public enum CreditCard {

    DINERS("Diners Club"),
    VISA("Visa"),
    MASTER("Master Card"),
    AE("American Express"),
    ELO("Elo Card"),
    HIPER("Hiper Card");

    public String name;

    CreditCard(String nome){
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
