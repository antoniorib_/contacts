package br.senai.sp.informatica.contacts.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Tecnico_Tarde on 23/03/2017.
 */

public class MessageUtil {
    public static void toast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
